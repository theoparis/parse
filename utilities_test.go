package pars

import (
	"fmt"
	"reflect"
	"testing"
)

func TestInto(t *testing.T) {
	r := stringReader("value=12345")

	var i int
	val, err := Seq(String("value="), Into(Int(), AssignInto(&i))).Parse(r)

	assertParseSlice(t, val, err, []interface{}{"value=", 12345}, nil)
	if i != 12345 {
		t.Errorf("Expected int 12345 in i, but found %v", i)
	}
}

func TestIntoUnread(t *testing.T) {
	r := stringReader("value=12345")
	expectedErr := fmt.Errorf("Could not find expected sequence item 2: EOF")

	var i = -1
	val, err := Seq(String("value="), Into(Int(), AssignInto(&i)), AnyRune()).Parse(r)

	assertParse(t, val, err, nil, expectedErr)
	if i != 0 {
		t.Errorf("Expected int 0 in i, but found %v", i)
	}
}

type pair struct{ key, val string }

var pairParser = func() Parser {
	var result pair
	return InsteadOfDeref(
		Seq(Into(DiscardRight(JoinString(RunesUntil(Char('='))), Char('=')), AssignInto(&result.key)),
			Into(JoinString(RunesUntil(Or(Char(','), EOF))), AssignInto(&result.val))),
		&result)
}

func TestInto_AssignIntoSlice(t *testing.T) {
	r := stringReader("a=1,b=2,c=3")

	var pairs []pair
	val, err := Into(Sep(pairParser(), Char(',')), AssignIntoSlice(&pairs)).Parse(r)
	assertParseSlice(t, val, err, []interface{}{pair{"a", "1"}, pair{"b", "2"}, pair{"c", "3"}}, nil)

	expected := []pair{{"a", "1"}, {"b", "2"}, {"c", "3"}}
	if !reflect.DeepEqual(pairs, expected) {
		t.Errorf("Expected %#v in pairs, but found %#v", expected, pairs)
	}
}

func TestIntoUnread_AssignIntoSlice(t *testing.T) {
	r := stringReader("a=1,b=2,c=3")

	var pairs []pair
	val, err := Or(Seq(Into(Sep(pairParser(), Char(',')), AssignIntoSlice(&pairs)), AnyRune()), JoinString(RunesUntil(EOF))).Parse(r)
	assertParse(t, val, err, "a=1,b=2,c=3", nil)

	if pairs != nil {
		t.Errorf("Expected pairs to be nil, but found %#v", pairs)
	}
}

func TestInsteadOf(t *testing.T) {
	r := stringReader("foo")

	val, err := InsteadOf(String("foo"), "bar").Parse(r)
	assertParse(t, val, err, "bar", nil)
}

func TestFailingInsteadOf(t *testing.T) {
	r := stringReader("baz")
	expectedErr := fmt.Errorf(`Could not parse expected string "foo": Unexpected string "baz"`)

	val, err := InsteadOf(String("foo"), "bar").Parse(r)
	assertParse(t, val, err, nil, expectedErr)
}

func TestInsteadOfDeref(t *testing.T) {
	r := stringReader("foo")

	target := "bar?"
	parser := InsteadOfDeref(String("foo"), &target)
	target = "bar!"

	val, err := parser.Parse(r)
	assertParse(t, val, err, "bar!", nil)
}

func TestFailingInsteadOfDeref(t *testing.T) {
	r := stringReader("baz")
	expectedErr := fmt.Errorf(`Could not parse expected string "foo": Unexpected string "baz"`)

	target := "bar?"
	parser := InsteadOfDeref(String("foo"), &target)

	val, err := parser.Parse(r)
	assertParse(t, val, err, nil, expectedErr)
}

func TestTransformer(t *testing.T) {
	r := stringReader("123")
	val, err := Transformer(Int(), func(v interface{}) (interface{}, error) { return v.(int) * 2, nil }).Parse(r)
	assertParse(t, val, err, 246, nil)
}

func TestFailingTransformer(t *testing.T) {
	r := stringReader("123")
	expectedErr := fmt.Errorf("Some transformer error")
	val, err := Transformer(Int(), func(v interface{}) (interface{}, error) { return nil, expectedErr }).Parse(r)
	assertParse(t, val, err, nil, expectedErr)

	expectedErr = fmt.Errorf("Some parser error")
	val, err = Transformer(DiscardRight(Int(), Error(expectedErr)), func(v interface{}) (interface{}, error) { return v, nil }).Parse(r)
	assertParse(t, val, err, nil, expectedErr)

	val, err = String("123").Parse(r)
	assertParse(t, val, err, "123", nil)
}

func TestTransformerUnread(t *testing.T) {
	r := stringReader("123")
	expectedErr := fmt.Errorf("Forced unread")
	val, err := DiscardRight(Transformer(Int(), func(v interface{}) (interface{}, error) { return v, nil }), Error(expectedErr)).Parse(r)
	assertParse(t, val, err, nil, expectedErr)

	val, err = String("123").Parse(r)
	assertParse(t, val, err, "123", nil)
}

func TestSwallowWhitespace(t *testing.T) {
	r := stringReader(" 123 ")
	val, err := DiscardRight(SwallowWhitespace(Int()), EOF).Parse(r)
	assertParse(t, val, err, 123, nil)
}

func TestSwallowLeadingWhitespace(t *testing.T) {
	r := stringReader(" 123 ")
	val, err := DiscardRight(SwallowLeadingWhitespace(Int()), Seq(String(" "), EOF)).Parse(r)
	assertParse(t, val, err, 123, nil)
}

func TestSwallowTrailingWhitespace(t *testing.T) {
	r := stringReader(" 123 ")
	val, err := DiscardLeft(String(" "), DiscardRight(SwallowTrailingWhitespace(Int()), EOF)).Parse(r)
	assertParse(t, val, err, 123, nil)
}

func TestErrorTransformer(t *testing.T) {
	r := stringReader("123")
	val, err := ErrorTransformer(Int(), func(e error) (interface{}, error) { return 0, nil }).Parse(r)
	assertParse(t, val, err, 123, nil)
}

func TestFailingErrorTransformer(t *testing.T) {
	r := stringReader("a123")
	expectedErr := fmt.Errorf("Some transformer error")
	val, err := ErrorTransformer(Int(), func(e error) (interface{}, error) { return nil, expectedErr }).Parse(r)
	assertParse(t, val, err, nil, expectedErr)

	val, err = ErrorTransformer(Int(), func(e error) (interface{}, error) { return "246", nil }).Parse(r)
	assertParse(t, val, err, "246", nil)

	val, err = String("a123").Parse(r)
	assertParse(t, val, err, "a123", nil)
}

func TestErrorTransformerUnread(t *testing.T) {
	r := stringReader("123")
	expectedErr := fmt.Errorf("Forced unread")
	val, err := DiscardRight(ErrorTransformer(Int(), func(e error) (interface{}, error) { return nil, e }), Error(expectedErr)).Parse(r)
	assertParse(t, val, err, nil, expectedErr)

	val, err = String("123").Parse(r)
	assertParse(t, val, err, "123", nil)
}

func TestJoinString(t *testing.T) {
	r := stringReader("abbcde")
	val, err := JoinString(Seq(AnyRune(), Char('b'), Seq(Char('b')), String("cd"), Some(Char('e')))).Parse(r)
	assertParse(t, val, err, "abbcde", nil)
}
