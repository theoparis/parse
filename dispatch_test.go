package pars

import (
	"fmt"
	"testing"
)

func TestEmptyDispatchFails(t *testing.T) {
	r := stringReader("")
	val, err := Dispatch().Parse(r)
	assertParse(t, val, err, nil, dispatchWithoutMatch{})
}

func TestNoMatchingClause(t *testing.T) {
	r := stringReader("a")
	expectedErr := fmt.Errorf("Last error is passed on")
	val, err := Dispatch(Clause{Char('b')}, Clause{Char('c')}, Clause{Error(expectedErr)}).Parse(r)
	assertParse(t, val, err, nil, expectedErr)
}

func TestSimpleMatchingClause(t *testing.T) {
	r := stringReader("a")
	val, err := Dispatch(Clause{Char('b')}, Clause{Char('a')}).Parse(r)
	assertParse(t, val, err, 'a', nil)
}

func TestMultiParserMatchingClause(t *testing.T) {
	r := stringReader("aAa")
	val, err := Dispatch(Clause{Char('b')}, Clause{Char('a'), Char('A'), Char('a')}).Parse(r)
	assertParse(t, nil, err, nil, nil)
	assertRunesInSlice(t, val.([]interface{}), "aAa")
}

func TestStringJoiningClause(t *testing.T) {
	r := stringReader("aAa")
	val, err := Dispatch(StringJoiningClause{Clause{Char('a'), Char('A'), Char('a')}}).Parse(r)
	assertParse(t, val, err, "aAa", nil)
}

func TestClauseSelection(t *testing.T) {
	r := stringReader("aAa")
	val, err := Dispatch(Clause{Char('a'), Char('b')}, Clause{Char('a'), Char('A'), Char('a')}).Parse(r)
	assertParse(t, val, err, nil, fmt.Errorf("Could not parse expected rune 'b' (0x62): Unexpected rune 'A' (0x41)"))
}

func TestErrorTransformingClause(t *testing.T) {
	r := stringReader("aAa")
	val, err := Dispatch(DescribeClause{DispatchClause: Clause{Char('a'), Char('b')}, Description: "ab"}).Parse(r)
	assertParse(t, val, err, nil, fmt.Errorf("ab expected: Could not parse expected rune 'b' (0x62): Unexpected rune 'A' (0x41)"))
}

func TestDispatchUnreadClause(t *testing.T) {
	r := stringReader("aAa")
	val, err := Dispatch(Clause{Char('a'), Char('A'), Char('a'), Char('A')}).Parse(r)
	assertParse(t, val, err, nil, fmt.Errorf("Could not parse expected rune 'A' (0x41): EOF"))

	val, err = String("aAa").Parse(r)
	assertParse(t, val, err, "aAa", nil)
}

func TestDispatchUnread(t *testing.T) {
	r := stringReader("aAa")
	val, err := Or(DiscardRight(Dispatch(Clause{Char('a'), Char('A')}), Char('b')), String("aAa")).Parse(r)
	assertParse(t, val, err, "aAa", nil)
}

func TestDispatchUnreadReusedClause(t *testing.T) {
	r := stringReader("aAaAa")
	clause := Clause{Char('a'), Char('A')}
	val, err := Or(Seq(Dispatch(clause), Dispatch(clause), Char('b')), String("aAaAa")).Parse(r)
	assertParse(t, val, err, "aAaAa", nil)
}

func TestDispatchSome(t *testing.T) {
	r := stringReader("aaaabbbaa")
	val, err := JoinString(DispatchSome(Clause{Char('a'), Char('a')}, Clause{Char('b')})).Parse(r)
	assertParse(t, val, err, "aaaabbbaa", nil)
}

func TestFailingDispatchSome(t *testing.T) {
	r := stringReader("aaaabbbab")
	expectedErr := fmt.Errorf("Could not parse expected rune 'a' (0x61): Unexpected rune 'b' (0x62)")
	val, err := JoinString(DispatchSome(Clause{Char('a'), Char('a')}, Clause{Char('b')})).Parse(r)
	assertParse(t, val, err, nil, expectedErr)
}

func TestDispatchSomeUnread(t *testing.T) {
	r := stringReader("aAaAaAaA")
	val, err := DiscardRight(DispatchSome(Clause{Char('a'), Char('A')}), Char('b')).Parse(r)
	expectedErr := fmt.Errorf("Could not parse expected rune 'b' (0x62): EOF")

	assertParse(t, val, err, nil, expectedErr)
	assertBytes(t, r.buf.current, []byte{})
	assertBytes(t, r.buf.prepend, []byte{65, 97, 65, 97, 65, 97, 65, 97})

	val, err = String("aAaAaAaA").Parse(r)
	assertParse(t, val, err, "aAaAaAaA", nil)
}

func TestInsteadOfClause(t *testing.T) {
	r := stringReader("aAa")
	val, err := JoinString(DispatchSome(Clause{Char('a')}, InsteadOfClause{DispatchClause: Clause{Char('A')}, Result: 'B'})).Parse(r)
	assertParse(t, val, err, "aBa", nil)
}

func TestInsteadOfDerefClause(t *testing.T) {
	r := stringReader("aAa")
	replacementLetter := 'B'
	parser := JoinString(DispatchSome(Clause{Char('a')}, InsteadOfDerefClause{DispatchClause: Clause{Char('A')}, Result: &replacementLetter}))
	replacementLetter = 'X'
	val, err := parser.Parse(r)
	assertParse(t, val, err, "aXa", nil)
}
