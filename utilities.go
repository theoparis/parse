package pars

import (
	"reflect"
	"strings"
	"unicode"
)

type intoParser struct {
	Parser
	setter       func(interface{})
	setterCalled bool
}

//Into wraps a parser so that a successful parse calls the given setter. If the wrapped parser fails, the setter is not called.
//If the returned parser succeeds at first, but is unread later, the setter will be called again with nil as the value.
//
//It is recommended that the variable the setter writes into is not read from other parsers (e.g. via Into or Transformer) as it might become
//confusing to understand which parser has seen which setter result at which time.
//
//Also, Into should be used as outmost as possible. A parser like 'Some(Into(AnyRune(), setter))' will have its setter called many times, so
//each value will be overwritten by the next one. On the other hand, you can use this for a setter that appends all values into a slice instead
//of just setting a single variable. Just have a clear idea of what your setter will do.
func Into(parser Parser, setter func(interface{})) Parser {
	return &intoParser{Parser: parser, setter: setter}
}

func (i *intoParser) Parse(src *Reader) (interface{}, error) {
	val, err := i.Parser.Parse(src)
	if err != nil {
		return nil, err
	}

	i.setter(val)
	i.setterCalled = true
	return val, nil
}

func (i *intoParser) Unread(src *Reader) {
	if i.setterCalled {
		i.setter(nil)
		i.setterCalled = false
	}

	i.Parser.Unread(src)
}

func (i *intoParser) Clone() Parser {
	return Into(i.Parser.Clone(), i.setter)
}

//AssignInto returns a setter for Into parsers. The setter will set a given field to the parsers result value on success and to the zero value on failure.
//
//This function or the generated setter might panic if the types do not fit.
func AssignInto(dest interface{}) func(interface{}) {
	destVal := reflect.ValueOf(dest)
	if !destVal.Elem().CanSet() {
		panic("AssignInto must be called with a pointer to a settable reflect.Value")
	}

	return func(val interface{}) {
		if val != nil {
			destVal.Elem().Set(reflect.ValueOf(val))
			return
		}

		destVal.Elem().Set(reflect.Zero(destVal.Elem().Type()))
	}
}

//AssignIntoSlice returns a setter for Into parsers. The setter will set a given slice pointer to the parsers result value on success and to nil on failure.
//
//This function or the generated setter might panic if the types do not fit.
func AssignIntoSlice(dest interface{}) func(interface{}) {
	destVal := reflect.ValueOf(dest)
	if !destVal.Elem().CanSet() || destVal.Elem().Kind() != reflect.Slice {
		panic("AssignIntoSlice must be called with a pointer to a settable reflect.Value of kind reflect.Slice")
	}

	return func(val interface{}) {
		if val != nil {
			valSlice := val.([]interface{})
			destSlice := reflect.MakeSlice(destVal.Elem().Type(), 0, len(valSlice))
			for _, valElem := range valSlice {
				destSlice = reflect.Append(destSlice, reflect.ValueOf(valElem).Convert(destVal.Elem().Type().Elem()))
			}
			destVal.Elem().Set(destSlice)
			return
		}

		destVal.Elem().Set(reflect.Zero(destVal.Elem().Type()))
	}
}

type insteadOfParser struct {
	Parser
	val interface{}
}

//InsteadOf wraps a parser so that a different given value is used as a result on success.
func InsteadOf(parser Parser, value interface{}) Parser {
	return &insteadOfParser{Parser: parser, val: value}
}

func (i *insteadOfParser) Parse(src *Reader) (interface{}, error) {
	_, err := i.Parser.Parse(src)
	if err != nil {
		return nil, err
	}

	return i.val, nil
}

func (i *insteadOfParser) Clone() Parser {
	return InsteadOf(i.Parser.Clone(), i.val)
}

type insteadOfDerefParser struct {
	Parser
	val interface{}
}

//InsteadOfDeref wraps a parser so that a different given value is used as a result on success. The given value has to be a pointer to something (not nil) and will be dereferenced.
func InsteadOfDeref(parser Parser, value interface{}) Parser {
	return &insteadOfDerefParser{Parser: parser, val: value}
}

func (i *insteadOfDerefParser) Parse(src *Reader) (interface{}, error) {
	_, err := i.Parser.Parse(src)
	if err != nil {
		return nil, err
	}

	return reflect.ValueOf(i.val).Elem().Interface(), nil
}

func (i *insteadOfDerefParser) Clone() Parser {
	return InsteadOfDeref(i.Parser.Clone(), i.val)
}

type transformingParser struct {
	Parser
	transformer func(interface{}) (interface{}, error)
	read        bool
}

//Transformer wraps a parser so that the result is transformed according to the given function. If the transformer returns an error, the parsing is handled as failed.
func Transformer(parser Parser, transformer func(interface{}) (interface{}, error)) Parser {
	return &transformingParser{Parser: parser, transformer: transformer}
}

func (t *transformingParser) Parse(src *Reader) (interface{}, error) {
	val, err := t.Parser.Parse(src)
	if err != nil {
		return nil, err
	}

	val, err = t.transformer(val)
	if err != nil {
		t.Parser.Unread(src)
		return nil, err
	}
	t.read = true
	return val, nil
}

func (t *transformingParser) Unread(src *Reader) {
	if t.read {
		t.Parser.Unread(src)
		t.read = false
	}
}

func (t *transformingParser) Clone() Parser {
	return Transformer(t.Parser.Clone(), t.transformer)
}

type errorTransformingParser struct {
	Parser
	transformer func(error) (interface{}, error)
	read        bool
}

//ErrorTransformer wraps a parser so that an error result is transformed according to the given function. If the wrapped parser was successful, the result is not changed.
func ErrorTransformer(parser Parser, transformer func(error) (interface{}, error)) Parser {
	return &errorTransformingParser{Parser: parser, transformer: transformer}
}

func (e *errorTransformingParser) Parse(src *Reader) (interface{}, error) {
	val, err := e.Parser.Parse(src)
	if err == nil {
		e.read = true
		return val, nil
	}

	val, err = e.transformer(err)
	return val, err
}

func (e *errorTransformingParser) Unread(src *Reader) {
	if e.read {
		e.Parser.Unread(src)
		e.read = false
	}
}

func (e *errorTransformingParser) Clone() Parser {
	return ErrorTransformer(e.Parser.Clone(), e.transformer)
}

//SwallowWhitespace wraps a parser so that it removes leading and trailing whitespace.
func SwallowWhitespace(parser Parser) Parser {
	return SwallowLeadingWhitespace(SwallowTrailingWhitespace(parser))
}

//SwallowLeadingWhitespace wraps a parser so that it removes leading whitespace.
func SwallowLeadingWhitespace(parser Parser) Parser {
	return DiscardLeft(Some(CharPred(unicode.IsSpace)), parser)
}

//SwallowTrailingWhitespace wraps a parser so that it removes trailing whitespace.
func SwallowTrailingWhitespace(parser Parser) Parser {
	return DiscardRight(parser, Some(CharPred(unicode.IsSpace)))
}

//JoinString wraps a parser that returns a slice of runes or strings so that it returns a single string instead.
//Runes and strings can be mixed in the same slice. The slice also can contain other slices of runes and strings,
//recursively.
//
//The returned parser WILL PANIC if the wrapped parser returns something that is not a slice of runes or strings!
func JoinString(parser Parser) Parser {
	return Transformer(parser, joinToString)
}

func joinToString(val interface{}) (interface{}, error) {
	vals := val.([]interface{})

	builder := strings.Builder{}
	for _, v := range vals {
		joinToStringHelper(&builder, v)
	}
	return builder.String(), nil
}

func joinToStringHelper(builder *strings.Builder, val interface{}) {
	if r, ok := val.(rune); ok {
		builder.WriteRune(r)
	} else if s, ok := val.(string); ok {
		builder.WriteString(s)
	} else if vals, ok := val.([]interface{}); ok {
		for _, v := range vals {
			joinToStringHelper(builder, v)
		}
	} else {
		panic(val)
	}
}
